var express = require("express");
var app = express();
const bodyParser = require('body-parser');

const casedata = {

};


app.listen(3000, () => {
    console.log("Server running on port 3000");
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/casedata/:workflowid", (req, res, next) => {
    console.log(casedata[req.params.workflowid]);
    res.json(casedata[req.params.workflowid]);
});

app.post('/casedata/:workflowid', (req, res) => {
    var workflowid = req.params.workflowid;
    console.log(workflowid);

    casedata[workflowid] = {"metadata": req.body};
    if(!casedata[workflowid].metadata) {
        casedata[workflowid].metadata = {}
    }

    casedata[workflowid].metadata = req.body;
    //casedata [workflowid].metadata = req.body;
    // Output the book to the console for debugging
    console.log(req.body);
    console.log(casedata);
    res.send(casedata);
});

app.put('/casedata/:workflowid', (req, res) => {
    var workflowid = req.params.workflowid;
    console.log(workflowid);

  
    if(!casedata[workflowid].metadata) {
        casedata[workflowid].metadata = {}
    }

    casedata[workflowid].metadata = req.body;
    //casedata [workflowid].metadata = req.body;
    // Output the book to the console for debugging
    console.log(req.body);
    console.log(casedata);
    res.send(casedata);
});


app.put('/casedata/:workflowid/document/:documenttype', (req, res) => {
    var workflowid = req.params.workflowid;

    var documenttype = req.params.documenttype;
    console.log("documenttype : " + documenttype);
    console.log(casedata);

    if (!casedata[workflowid].documents) {
        casedata[workflowid].documents = {};
    }
    console.log(casedata);
    casedata[workflowid].documents[documenttype] = req.body;
    // Output the book to the console for debugging
    console.log(casedata);
    res.send(casedata);
});