package no.dnb.process.sample.casedata.domain.documents;

public class PostEligibilityDocument extends CaseDataDocument<CaseDataDocumentType> {
    String customerBehaviour;
    String productEligibility;

    public PostEligibilityDocument() {

    }
    public PostEligibilityDocument(String customerBehaviour, String productEligibility) {
        this.productEligibility = productEligibility;
        this.customerBehaviour = customerBehaviour;
        super.documentType = CaseDataDocumentType.POST_ELIGIBILITY_DOCUMENT;
    }

}

