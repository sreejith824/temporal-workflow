package no.dnb.process.sample.casedata.domain.documents;

import no.dnb.process.sample.customeronboardingworkflow.domain.CustomerEvaluation;


public class DocumentPreparationDocument extends CaseDataDocument<CaseDataDocumentType> {
    String eSignUrl;
    String documentTemplate;

    public DocumentPreparationDocument(String eSignUrl, String documentTemplate) {
        this.eSignUrl = eSignUrl;
        this.documentTemplate = documentTemplate;
        super.documentType = CaseDataDocumentType.DOCUMENT_PREPARATION_DOCUMENT;
    }

    public DocumentPreparationDocument() {

    }

}
