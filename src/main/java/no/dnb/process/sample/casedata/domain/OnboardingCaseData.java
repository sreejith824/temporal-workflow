package no.dnb.process.sample.casedata.domain;


import java.sql.Timestamp;

public class OnboardingCaseData
 {
     private String name;
     private String ssn;
     private String productName;
     private Timestamp createdTimestamp;
     private Timestamp lastUpdatedTimeStamp;

     private String applicationStatus;
     private String caseStatus;

     private String workflowId;

     public String getName() {
         return name;
     }

     public void setName(String name) {
         this.name = name;
     }

     public String getSsn() {
         return ssn;
     }

     public void setSsn(String ssn) {
         this.ssn = ssn;
     }

     public String getProductName() {
         return productName;
     }

     public void setProductName(String productName) {
         this.productName = productName;
     }

     public Timestamp getCreatedTimestamp() {
         return createdTimestamp;
     }

     public void setCreatedTimestamp(Timestamp createdTimestamp) {
         this.createdTimestamp = createdTimestamp;
     }

     public Timestamp getLastUpdatedTimeStamp() {
         return lastUpdatedTimeStamp;
     }

     public void setLastUpdatedTimeStamp(Timestamp lastUpdatedTimeStamp) {
         this.lastUpdatedTimeStamp = lastUpdatedTimeStamp;
     }

     public String getApplicationStatus() {
         return applicationStatus;
     }

     public void setApplicationStatus(String applicationStatus) {
         this.applicationStatus = applicationStatus;
     }

     public String getCaseStatus() {
         return caseStatus;
     }

     public void setCaseStatus(String caseStatus) {
         this.caseStatus = caseStatus;
     }

     public String getWorkflowId() {
         return workflowId;
     }

     public void setWorkflowId(String workflowId) {
         this.workflowId = workflowId;
     }
 }
