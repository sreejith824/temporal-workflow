package no.dnb.process.sample.casedata.domain.documents;

public enum CaseDataDocumentType {

    POST_ELIGIBILITY_DOCUMENT("PostEligibilityDocument"),
    CUSTOMER_EVALUATION_DOCUMENT("CustomerEvaluationDocument"),
    DOCUMENT_PREPARATION_DOCUMENT("DocumentPreparationDocument"),
    ACCOUNT_FULFILLMENT_DOCUMENT("AccountFulfillmentDocument"),
    CARD_FULFILLMENT_DOCUMENT("CardFulfillmentDocument");



    private final String type;

    CaseDataDocumentType(String status) {
        this.type = status;

    }

    public String getType() {
        return type;
    }
}
