package no.dnb.process.sample.casedata.domain.documents;

import no.dnb.process.sample.customeronboardingworkflow.domain.CustomerEvaluation;

public class CustomerEvaluationDocument extends CaseDataDocument<CaseDataDocumentType> {
    CustomerEvaluation customerEvaluation;
    String score;

    public CustomerEvaluationDocument(CustomerEvaluation customerEvaluation, String score) {
        this.customerEvaluation = customerEvaluation;
        this.score = score;
        super.documentType = CaseDataDocumentType.CUSTOMER_EVALUATION_DOCUMENT;
    }
    public CustomerEvaluationDocument() {

    }

}
