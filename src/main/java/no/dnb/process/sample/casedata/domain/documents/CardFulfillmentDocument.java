package no.dnb.process.sample.casedata.domain.documents;


public class CardFulfillmentDocument extends CaseDataDocument<CaseDataDocumentType> {
    String cardNumber;

    public CardFulfillmentDocument(String cardNumber) {
        this.cardNumber = cardNumber;
        super.documentType = CaseDataDocumentType.CARD_FULFILLMENT_DOCUMENT;
    }

    public CardFulfillmentDocument() {

    }

}