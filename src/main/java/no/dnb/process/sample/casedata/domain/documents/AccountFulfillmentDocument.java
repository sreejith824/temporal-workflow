package no.dnb.process.sample.casedata.domain.documents;

public class AccountFulfillmentDocument extends CaseDataDocument<CaseDataDocumentType> {
    String accountNumber;

    public AccountFulfillmentDocument(String accountNumber) {
        this.accountNumber = accountNumber;
        super.documentType = CaseDataDocumentType.ACCOUNT_FULFILLMENT_DOCUMENT;
    }

    public AccountFulfillmentDocument() {

    }

}
