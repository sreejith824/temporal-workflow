package no.dnb.process.sample.casedata.domain.documents;

public abstract class  CaseDataDocument<CaseDataDocumentType> {

    CaseDataDocumentType documentType;

    public CaseDataDocumentType getDocumentType() {
        return this.documentType;
    };

    public CaseDataDocument () {

    }
}
