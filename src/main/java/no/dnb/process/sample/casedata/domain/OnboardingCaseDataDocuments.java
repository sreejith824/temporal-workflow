package no.dnb.process.sample.casedata.domain;

import no.dnb.process.sample.casedata.domain.documents.*;

public class OnboardingCaseDataDocuments {

    PostEligibilityDocument postEligibilityDocument;
    CustomerEvaluationDocument customerEvaluationDocument;
    DocumentPreparationDocument documentPreparationDocument;
    AccountFulfillmentDocument accountFulfillmentDocument;
    CardFulfillmentDocument cardFulfillmentDocument;

    public PostEligibilityDocument getPostEligibility() {
        return postEligibilityDocument;
    }

    public void setPostEligibility(PostEligibilityDocument postEligibilityDocument) {
        this.postEligibilityDocument = postEligibilityDocument;
    }

    public CustomerEvaluationDocument getCustomerEvaluation() {
        return customerEvaluationDocument;
    }

    public void setCustomerEvaluation(CustomerEvaluationDocument customerEvaluationDocument) {
        this.customerEvaluationDocument = customerEvaluationDocument;
    }

    public DocumentPreparationDocument getDocumentPreparation() {
        return documentPreparationDocument;
    }

    public void setDocumentPreparation(DocumentPreparationDocument documentPreparationDocument) {
        this.documentPreparationDocument = documentPreparationDocument;
    }

    public AccountFulfillmentDocument getAccountFulfillment() {
        return accountFulfillmentDocument;
    }

    public void setAccountFulfillment(AccountFulfillmentDocument accountFulfillmentDocument) {
        this.accountFulfillmentDocument = accountFulfillmentDocument;
    }

    public CardFulfillmentDocument getCardFulfillment() {
        return cardFulfillmentDocument;
    }

    public void setCardFulfillment(CardFulfillmentDocument cardFulfillmentDocument) {
        this.cardFulfillmentDocument = cardFulfillmentDocument;
    }
}
