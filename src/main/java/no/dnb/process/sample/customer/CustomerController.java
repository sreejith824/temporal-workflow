package no.dnb.process.sample.customer;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("api/v1/customer")
@RestController
public class CustomerController {
    @GetMapping("/{ssn}")
    public ResponseEntity<String> getCustomer(@PathVariable("ssn") long ssn) {
        return new ResponseEntity<>("Customer Details of SSN : " + ssn, HttpStatus.OK);
    }

    @GetMapping("/{ssn}/behaviour")
    public ResponseEntity<String> getCustomerBehaviour(@PathVariable("ssn") long ssn) {
        return new ResponseEntity<>("Customer Behaviour Details of SSN : " + ssn, HttpStatus.OK);
    }
}
