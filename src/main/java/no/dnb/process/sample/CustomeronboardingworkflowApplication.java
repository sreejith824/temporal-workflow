package no.dnb.process.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomeronboardingworkflowApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomeronboardingworkflowApplication.class, args);
	}

}
