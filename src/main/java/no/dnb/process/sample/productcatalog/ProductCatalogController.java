package no.dnb.process.sample.productcatalog;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("api/v1/productcatalog")
@RestController
public class ProductCatalogController {
    @GetMapping("/{productName}")
    public ResponseEntity<String> checkProductEligibility(@PathVariable("productName") String productName) {
        return new ResponseEntity<>("Product is available " + productName, HttpStatus.OK);
    }

}
