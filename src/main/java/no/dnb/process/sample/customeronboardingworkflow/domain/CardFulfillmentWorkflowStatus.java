package no.dnb.process.sample.customeronboardingworkflow.domain;

public enum CardFulfillmentWorkflowStatus {
    CARD_CREATION_IN_PROGRESS("CARD_CREATION_IN_PROGRESS"),

    CARD_CREATION_COMPLETED("CARD_CREATION_COMPLETED");

    private final String status;

    CardFulfillmentWorkflowStatus(String status) {
        this.status = status;

    }

    public String getStatus() {
        return status;
    }
}
