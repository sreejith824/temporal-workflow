package no.dnb.process.sample.customeronboardingworkflow.domain;

public enum AccountFulfillmentWorkflowStatus {
    ACCOUNT_CREATION_IN_PROGRESS("ACCOUNT_CREATION_IN_PROGRESS"),

    ACCOUNT_CREATION_COMPLETED("ACCOUNT_CREATION_COMPLETED");

    private final String status;

    AccountFulfillmentWorkflowStatus(String status) {
        this.status = status;

    }

    public String getStatus() {
        return status;
    }
}
