package no.dnb.process.sample.customeronboardingworkflow.activity;

import no.dnb.process.sample.customeronboardingworkflow.domain.CustomerEvaluation;

import java.util.UUID;

public class CustomerScoringActivityImpl implements CustomerScoringActivity {


    @Override
    public String submitForCustomerScoring(CustomerEvaluation customerEvaluation) {
        String id = UUID.randomUUID().toString();
        System.out.println("submitForCustomerScoring for id : " + id);
        return id;
    }

    @Override
    public String getCustomerScoring(String id) {
        System.out.println("getCustomerScoring for id : " + id);
        return "APPROVED";
    }
}
