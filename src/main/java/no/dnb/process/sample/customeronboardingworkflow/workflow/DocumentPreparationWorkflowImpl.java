package no.dnb.process.sample.customeronboardingworkflow.workflow;

import io.temporal.activity.ActivityOptions;
import io.temporal.workflow.Workflow;
import no.dnb.process.sample.casedata.domain.OnboardingCaseData;
import no.dnb.process.sample.casedata.domain.OnboardingCaseDataDocuments;
import no.dnb.process.sample.casedata.domain.documents.CustomerEvaluationDocument;
import no.dnb.process.sample.casedata.domain.documents.DocumentPreparationDocument;
import no.dnb.process.sample.customeronboardingworkflow.activity.CaseDataActivity;
import no.dnb.process.sample.customeronboardingworkflow.activity.DocumentProductionActivity;
import no.dnb.process.sample.customeronboardingworkflow.domain.DocumentPreparationWorkflowStatus;

import java.time.Duration;

public class DocumentPreparationWorkflowImpl implements DocumentPreparationWorkflow {
    final DocumentProductionActivity documentProductionActivity =
            Workflow.newActivityStub(
                    DocumentProductionActivity.class,
                    ActivityOptions.newBuilder().setStartToCloseTimeout(Duration.ofSeconds(2)).build());
    final CaseDataActivity caseDataActivity =
            Workflow.newActivityStub(
                    CaseDataActivity.class,
                    ActivityOptions.newBuilder().setStartToCloseTimeout(Duration.ofSeconds(2)).build());

    String status;

    @Override
    public void executeDocumentPreparation(String name, String ssn, String productName, OnboardingCaseData onboardingCaseData) {
        status = DocumentPreparationWorkflowStatus.DOCUMENT_PREPARATION_STARTED.getStatus();
        WorkflowUtil.updateParentWorkflowStatus(status);
        System.out.println(documentProductionActivity.produceDocument(name, ssn, productName));
        status = DocumentPreparationWorkflowStatus.DOCUMENT_PREPARATION_COMPLETED.getStatus();
        WorkflowUtil.updateParentWorkflowStatus(status);
        String parentWorkflowId = Workflow.getInfo().getParentWorkflowId().get();
        OnboardingCaseDataDocuments onboardingCaseDataDocuments = new OnboardingCaseDataDocuments();
        onboardingCaseDataDocuments.setDocumentPreparation(new DocumentPreparationDocument("https://esignurl", "KUNDE_AVTALE"));
        caseDataActivity.updateDocument(onboardingCaseDataDocuments, parentWorkflowId);
    }


    @Override
    public String getStatus() {
        return null;
    }
}
