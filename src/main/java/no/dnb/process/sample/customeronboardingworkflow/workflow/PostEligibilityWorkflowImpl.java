package no.dnb.process.sample.customeronboardingworkflow.workflow;

import io.temporal.activity.ActivityOptions;
import io.temporal.workflow.Workflow;
import no.dnb.process.sample.casedata.domain.OnboardingCaseData;
import no.dnb.process.sample.casedata.domain.OnboardingCaseDataDocuments;
import no.dnb.process.sample.casedata.domain.documents.PostEligibilityDocument;
import no.dnb.process.sample.customeronboardingworkflow.activity.CaseDataActivity;
import no.dnb.process.sample.customeronboardingworkflow.activity.CustomerDetailsActivity;
import no.dnb.process.sample.customeronboardingworkflow.activity.ProductDetailsActivity;
import no.dnb.process.sample.customeronboardingworkflow.domain.PostEligibilityWorkflowStatus;

import java.time.Duration;

public class PostEligibilityWorkflowImpl implements PostEligibilityWorkflow {
    final CustomerDetailsActivity customerDetailsActivity =
            Workflow.newActivityStub(
                    CustomerDetailsActivity.class,
                    ActivityOptions.newBuilder().setStartToCloseTimeout(Duration.ofSeconds(2)).build());
    final ProductDetailsActivity productDetailsActivity =
            Workflow.newActivityStub(
                    ProductDetailsActivity.class,
                    ActivityOptions.newBuilder().setStartToCloseTimeout(Duration.ofSeconds(2)).build());
    final CaseDataActivity caseDataActivity =
            Workflow.newActivityStub(
                    CaseDataActivity.class,
                    ActivityOptions.newBuilder().setStartToCloseTimeout(Duration.ofSeconds(2)).build());

    String status;

    @Override
    public void executePostEligibility(String name, String ssn, String productName, OnboardingCaseData onboardingCaseData) {
        String productEligibility = null;
        status =  PostEligibilityWorkflowStatus.POST_ELIGIBILITY_STARTED.getStatus();
        WorkflowUtil.updateParentWorkflowStatus(status);
        String customerBehaviour = customerDetailsActivity.getCustomerBehaviour(name, ssn);
        if (null != customerBehaviour) {
            productEligibility = productDetailsActivity.checkProductEligibility(productName);
            if (null != productEligibility) {
                System.out.println("Customer with SSN " + ssn + " is eligible for " + productName);
                status =  PostEligibilityWorkflowStatus.POST_ELIGIBILITY_PENDING.getStatus();
                WorkflowUtil.updateParentWorkflowStatus(status);
            }
        }
        Workflow.await(Duration.ofMinutes(1),() -> false);
        status =  PostEligibilityWorkflowStatus.POST_ELIGIBILITY_COMPLETED.getStatus();
        System.out.println("Signal parent : " + status);
        WorkflowUtil.updateParentWorkflowStatus(status);
        String parentWorkflowId = Workflow.getInfo().getParentWorkflowId().get();
        OnboardingCaseDataDocuments onboardingCaseDataDocuments = new OnboardingCaseDataDocuments();
        onboardingCaseDataDocuments.setPostEligibility(new PostEligibilityDocument(customerBehaviour, productEligibility));
        caseDataActivity.updateDocument(onboardingCaseDataDocuments, parentWorkflowId);
    }

    @Override
    public String getStatus() {
        return status;
    }


}
