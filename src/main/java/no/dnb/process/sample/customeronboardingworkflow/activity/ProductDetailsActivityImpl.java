package no.dnb.process.sample.customeronboardingworkflow.activity;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class ProductDetailsActivityImpl implements ProductDetailsActivity {
    OkHttpClient client = new OkHttpClient();
    final String CHECK_PRODUCT_ELIGIBILITY_URL = "http://localhost:9090/pm-digital/api/v1/productcatalog/SAVINGS";
    @Override
    public String checkProductEligibility(String productName) {
        Request request = new Request.Builder()
                .url(CHECK_PRODUCT_ELIGIBILITY_URL)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
