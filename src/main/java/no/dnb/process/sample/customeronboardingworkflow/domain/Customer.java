package no.dnb.process.sample.customeronboardingworkflow.domain;

public record Customer (String name, String ssn, String productName) {
}
