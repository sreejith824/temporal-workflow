package no.dnb.process.sample.customeronboardingworkflow.domain;

public enum DocumentPreparationWorkflowStatus {
    DOCUMENT_PREPARATION_STARTED("DOCUMENT_PREPARATION_STARTED"),

    DOCUMENT_PREPARATION_PENDING("DOCUMENT_PREPARATION_PENDING"),
    DOCUMENT_PREPARATION_COMPLETED("DOCUMENT_PREPARATION_COMPLETED"),
    DOCUMENT_PREPARATION_CANCELLED("DOCUMENT_PREPARATION_CANCELLED"),
    DOCUMENT_PREPARATION_FAILED("DOCUMENT_PREPARATION_FAILED");

    private final String status;

    DocumentPreparationWorkflowStatus(String status) {
        this.status = status;

    }

    public String getStatus() {
        return status;
    }
}
