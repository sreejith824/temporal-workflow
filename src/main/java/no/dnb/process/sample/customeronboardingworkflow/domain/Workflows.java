package no.dnb.process.sample.customeronboardingworkflow.domain;

public enum Workflows {
    ONBOARDING("OnboardingWorkflow"),

    POST_ELIGIBILITY("PostEligibilityWorkflow"),
    CUSTOMER_EVALUATION("CustomerEvaluationWorkflow");

    private final String workflow;

    Workflows(String workflow) {
        this.workflow = workflow;

    }

    public String getWorkflow() {
        return workflow;
    }
}
