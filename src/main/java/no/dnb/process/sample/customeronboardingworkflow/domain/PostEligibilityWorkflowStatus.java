package no.dnb.process.sample.customeronboardingworkflow.domain;

public enum PostEligibilityWorkflowStatus {
    POST_ELIGIBILITY_STARTED("POST_ELIGIBILITY_STARTED"),

    POST_ELIGIBILITY_PENDING("POST_ELIGIBILITY_PENDING"),
    POST_ELIGIBILITY_COMPLETED("POST_ELIGIBILITY_COMPLETED"),
    POST_ELIGIBILITY_CANCELLED("POST_ELIGIBILITY_CANCELLED"),
    POST_ELIGIBILITY_FAILED("POST_ELIGIBILITY_FAILED");

    private final String status;

    PostEligibilityWorkflowStatus(String status) {
        this.status = status;

    }

    public String getStatus() {
        return status;
    }
}
