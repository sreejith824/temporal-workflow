package no.dnb.process.sample.customeronboardingworkflow.domain;

public enum CustomerEvaluationWorkflowStatus {
    CUSTOMER_EVALUATION_STARTED("CUSTOMER_EVALUATION_STARTED"),

    AML_SCORING_IN_PROGRESS("AML_SCORING_IN_PROGRESS"),
    CUSTOMER_EVALUATION_COMPLETED("CUSTOMER_EVALUATION_COMPLETED"),
    CUSTOMER_EVALUATION_CANCELLED("CUSTOMER_EVALUATION_CANCELLED"),
    CUSTOMER_EVALUATION_FAILED("CUSTOMER_EVALUATION_FAILED");

    private final String status;

    CustomerEvaluationWorkflowStatus(String status) {
        this.status = status;

    }

    public String getStatus() {
        return status;
    }
}
