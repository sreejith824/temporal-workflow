package no.dnb.process.sample.customeronboardingworkflow.activity;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;
import no.dnb.process.sample.customeronboardingworkflow.domain.CustomerEvaluation;

@ActivityInterface
public interface CustomerScoringActivity {
    @ActivityMethod
    String submitForCustomerScoring(CustomerEvaluation customerEvaluation);
    String getCustomerScoring(String id);
}
