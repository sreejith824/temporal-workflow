package no.dnb.process.sample.customeronboardingworkflow.workflow;

import io.temporal.workflow.QueryMethod;
import io.temporal.workflow.SignalMethod;
import io.temporal.workflow.WorkflowInterface;
import io.temporal.workflow.WorkflowMethod;
import no.dnb.process.sample.customeronboardingworkflow.domain.Customer;
import no.dnb.process.sample.customeronboardingworkflow.domain.CustomerEvaluation;

@WorkflowInterface
public interface OnboardingWorkflow {
    @WorkflowMethod
    String onboardCustomer(Customer customer);
    @QueryMethod
    String getStatus();

    @SignalMethod(name = "Update Status")
    void updateStatus(String status);

    @SignalMethod(name = "KYC_PENDING")
    void submitKYC(CustomerEvaluation customerEvaluation);

    @SignalMethod(name = "USER_CONFIRMATION_PENDING")
    void confirmAccount(boolean isAccountConfirmed);

    @SignalMethod(name = "WAITING_FOR_ESIGN")
    void eSignDocument(boolean isDocumentESigned);

}
