package no.dnb.process.sample.customeronboardingworkflow.activity;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class CustomerDetailsActivityImpl implements CustomerDetailsActivity {
    OkHttpClient client = new OkHttpClient();
    final String GET_CUSTOMER_BEHAVIOUR_URL = "http://localhost:9090/pm-digital/api/v1/customer/12144/behaviour";
    final String GET_CUSTOMER_URL = "http://localhost:9090/pm-digital/api/v1/customer/12144";
    @Override
    public String getCustomerBehaviour(String name, String ssn) {
        Request request = new Request.Builder()
                .url(GET_CUSTOMER_BEHAVIOUR_URL)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }



    @Override
    public String getCustomerDetails(String name, String ssn) {
        Request request = new Request.Builder()
                .url(GET_CUSTOMER_URL)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
