package no.dnb.process.sample.customeronboardingworkflow.workflow;

import io.temporal.workflow.ExternalWorkflowStub;
import io.temporal.workflow.Workflow;

public  class WorkflowUtil {

    public static void updateParentWorkflowStatus(String status) {
        ExternalWorkflowStub callRespondWorkflow = Workflow.newUntypedExternalWorkflowStub(Workflow.getInfo().getParentWorkflowId().get());
        System.out.println("Update parent with status : " + status + " from workflow : " + Workflow.getInfo().getWorkflowType());
        callRespondWorkflow.signal("Update Status", status);
    }
}
