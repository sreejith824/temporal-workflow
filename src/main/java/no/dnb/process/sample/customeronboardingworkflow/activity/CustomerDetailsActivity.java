package no.dnb.process.sample.customeronboardingworkflow.activity;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;

@ActivityInterface
public interface CustomerDetailsActivity {
    @ActivityMethod
    String getCustomerBehaviour(String name, String ssn);
    @ActivityMethod
    String getCustomerDetails(String name, String ssn);
}
