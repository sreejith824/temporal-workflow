package no.dnb.process.sample.customeronboardingworkflow.activity;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;

@ActivityInterface
public interface DocumentProductionActivity {
    @ActivityMethod
    String produceDocument(String name, String ssn, String productName);
}
