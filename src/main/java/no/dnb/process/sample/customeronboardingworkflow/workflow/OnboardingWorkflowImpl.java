package no.dnb.process.sample.customeronboardingworkflow.workflow;


import io.temporal.activity.ActivityOptions;
import io.temporal.workflow.ChildWorkflowOptions;
import io.temporal.workflow.Workflow;
import no.dnb.process.sample.casedata.domain.OnboardingCaseData;
import no.dnb.process.sample.customeronboardingworkflow.activity.CaseDataActivity;
import no.dnb.process.sample.customeronboardingworkflow.activity.CustomerDetailsActivity;
import no.dnb.process.sample.customeronboardingworkflow.activity.CustomerScoringActivity;
import no.dnb.process.sample.customeronboardingworkflow.domain.*;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class OnboardingWorkflowImpl implements OnboardingWorkflow {
    final CustomerDetailsActivity customerDetailsActivity =
            Workflow.newActivityStub(
                    CustomerDetailsActivity.class,
                    ActivityOptions.newBuilder().setStartToCloseTimeout(Duration.ofSeconds(2)).build());
    final CustomerScoringActivity customerScoringActivity =
            Workflow.newActivityStub(
                    CustomerScoringActivity.class,
                    ActivityOptions.newBuilder().setStartToCloseTimeout(Duration.ofSeconds(2)).build());

    final CaseDataActivity caseDataActivity =
            Workflow.newActivityStub(
                    CaseDataActivity.class,
                    ActivityOptions.newBuilder().setStartToCloseTimeout(Duration.ofSeconds(2)).build());
    CustomerEvaluation customerEvaluation = null;

    boolean isAccountConfirmed = false;
    boolean isDocumentESigned = false;
    String status = CustomerOnboardingWorkflowStatus.CUSTOMER_ONBOARDING_STARTED.getStatus();


    @Override
    public String onboardCustomer(Customer customer) {

        String name = customer.name();
        String ssn = customer.ssn();
        String productName = customer.productName();
        Map<String, Object> attr1 = new HashMap<>();
        attr1.put("CustomStringField", customer.ssn());
        Workflow.upsertSearchAttributes(attr1);
        String workflowId = Workflow.getInfo().getWorkflowId();
        OnboardingCaseData onboardingCaseData = createCaseData(customer, workflowId);

        String customerDetails = customerDetailsActivity.getCustomerDetails(name, ssn);
        handlePostEligibilityWorkflow(ssn, productName, customerDetails, onboardingCaseData);
        if (PostEligibilityWorkflowStatus.POST_ELIGIBILITY_FAILED.equals(status)) {
            return Workflow.getInfo().getWorkflowId();
        }
        updateCaseData(onboardingCaseData);

        status = CustomerOnboardingWorkflowStatus.KYC_PENDING.getStatus();
        updateCaseData(onboardingCaseData);
        Workflow.await(Duration.ofMinutes(5), () -> customerEvaluation != null);
        handleCustomerEvaluationWorkflow(customerEvaluation, onboardingCaseData);
        if (CustomerEvaluationWorkflowStatus.CUSTOMER_EVALUATION_FAILED.equals(status)) {
            return Workflow.getInfo().getWorkflowId();
        }
        updateCaseData(onboardingCaseData);

        status = CustomerOnboardingWorkflowStatus.USER_CONFIRMATION_PENDING.getStatus();
        updateCaseData(onboardingCaseData);
        Workflow.await(Duration.ofMinutes(5), () -> isAccountConfirmed);

        handleDocumentPreparationWorkflow(name, ssn, productName, onboardingCaseData);
        status = CustomerOnboardingWorkflowStatus.WAITING_FOR_ESIGN.getStatus();
        updateCaseData(onboardingCaseData);
        Workflow.await(Duration.ofMinutes(5), () -> isDocumentESigned);
        updateCaseData(onboardingCaseData);

        handleAccountFulfillmentWorkflow(customer, onboardingCaseData);
        updateCaseData(onboardingCaseData);

        handleCardFulfillmentWorkflow(customer, onboardingCaseData);
        updateCaseData(onboardingCaseData);

        status = status.equals(CardFulfillmentWorkflowStatus.CARD_CREATION_COMPLETED.getStatus()) ?
                CustomerOnboardingWorkflowStatus.CUSTOMER_ONBOARDING_COMPLETED.getStatus() :
                CustomerOnboardingWorkflowStatus.CUSTOMER_ONBOARDING_FAILED.getStatus();
        closeCaseData(onboardingCaseData);

        return Workflow.getInfo().getWorkflowId();

    }

    private void updateCaseData(OnboardingCaseData onboardingCaseData) {
        onboardingCaseData.setApplicationStatus(status);
        onboardingCaseData.setLastUpdatedTimeStamp(Timestamp.from(Instant.now()));
        caseDataActivity.updateCaseData(onboardingCaseData);
    }

    private void closeCaseData(OnboardingCaseData onboardingCaseData) {
        onboardingCaseData.setApplicationStatus(status);
        onboardingCaseData.setCaseStatus("Closed");
        onboardingCaseData.setLastUpdatedTimeStamp(Timestamp.from(Instant.now()));
        caseDataActivity.updateCaseData(onboardingCaseData);
    }

    private OnboardingCaseData createCaseData(Customer customer, String workflowId) {
        OnboardingCaseData onboardingCaseData = new OnboardingCaseData();
        onboardingCaseData.setCreatedTimestamp(Timestamp.from(Instant.now()));
        onboardingCaseData.setName(customer.name());
        onboardingCaseData.setSsn(customer.ssn());
        onboardingCaseData.setProductName(customer.productName());
        onboardingCaseData.setLastUpdatedTimeStamp(Timestamp.from(Instant.now()));
        onboardingCaseData.setCaseStatus("In progress");
        onboardingCaseData.setWorkflowId(workflowId);
        caseDataActivity.createCaseData(onboardingCaseData);
        return onboardingCaseData;
    }

    private void handlePostEligibilityWorkflow(String ssn, String productName, String customerDetails, OnboardingCaseData onboardingCaseData) {
        PostEligibilityWorkflow postEligibilityChildWorkflow = Workflow.newChildWorkflowStub(PostEligibilityWorkflow.class,
                ChildWorkflowOptions.newBuilder()
                        .setNamespace("default")
                        .setWorkflowExecutionTimeout(Duration.ofMinutes(5))
                        .build());
        postEligibilityChildWorkflow.executePostEligibility(customerDetails, ssn, productName, onboardingCaseData);
    }

    private void handleAccountFulfillmentWorkflow(Customer customer, OnboardingCaseData onboardingCaseData) {
        AccountFulfillmentWorkflow accountFulfillmentWorkflow = Workflow.newChildWorkflowStub(AccountFulfillmentWorkflow.class,
                ChildWorkflowOptions.newBuilder()
                        .setNamespace("default")
                        .setWorkflowExecutionTimeout(Duration.ofMinutes(5))
                        .build());
        accountFulfillmentWorkflow.executeAccountFulfillment(customer, onboardingCaseData);
    }

    private void handleCardFulfillmentWorkflow(Customer customer, OnboardingCaseData onboardingCaseData) {
        CardFulfillmentWorkflow cardFulfillmentWorkflow = Workflow.newChildWorkflowStub(CardFulfillmentWorkflow.class,
                ChildWorkflowOptions.newBuilder()
                        .setNamespace("default")
                        .setWorkflowExecutionTimeout(Duration.ofMinutes(5))
                        .build());
        cardFulfillmentWorkflow.executeAccountFulfillment(customer, onboardingCaseData);
    }


    private void handleDocumentPreparationWorkflow(String name, String ssn, String productName, OnboardingCaseData onboardingCaseData) {
        DocumentPreparationWorkflow documentPreparationWorkflow = Workflow.newChildWorkflowStub(DocumentPreparationWorkflow.class,
                ChildWorkflowOptions.newBuilder()
                        .setNamespace("default")
                        .setWorkflowExecutionTimeout(Duration.ofMinutes(5))
                        .build());
        documentPreparationWorkflow.executeDocumentPreparation(name, ssn, productName, onboardingCaseData);
    }

    private void handleCustomerEvaluationWorkflow(CustomerEvaluation customerEvaluation, OnboardingCaseData onboardingCaseData) {
        CustomerEvaluationWorkflow customerEvaluationWorkflow = Workflow.newChildWorkflowStub(CustomerEvaluationWorkflow.class,
                ChildWorkflowOptions.newBuilder()
                        .setNamespace("default")
                        .setWorkflowExecutionTimeout(Duration.ofMinutes(5))
                        .build());
        customerEvaluationWorkflow.executeCustomerEvaluation(customerEvaluation, onboardingCaseData);
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void updateStatus(String status) {
        System.out.println("Signal received : " + status);
        this.status = status;
    }

    @Override
    public void submitKYC(CustomerEvaluation customerEvaluation) {
        this.customerEvaluation = customerEvaluation;
    }

    @Override
    public void confirmAccount(boolean isAccountConfirmed) {
        this.isAccountConfirmed = isAccountConfirmed;
    }

    @Override
    public void eSignDocument(boolean isDocumentESigned) {
        this.isDocumentESigned = isDocumentESigned;
    }

}
