package no.dnb.process.sample.customeronboardingworkflow.activity;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;
import no.dnb.process.sample.customeronboardingworkflow.domain.Customer;

@ActivityInterface
public interface CardDetailsActivity {
    @ActivityMethod
    String createCard(Customer customer);
}
