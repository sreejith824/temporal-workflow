package no.dnb.process.sample.customeronboardingworkflow.activity;

import com.google.gson.Gson;
import io.temporal.workflow.Workflow;
import no.dnb.process.sample.casedata.domain.OnboardingCaseData;
import no.dnb.process.sample.casedata.domain.OnboardingCaseDataDocuments;
import no.dnb.process.sample.casedata.domain.documents.CaseDataDocument;
import okhttp3.*;

import java.io.IOException;

public class CaseDataActivityImpl implements CaseDataActivity {


    OkHttpClient client = new OkHttpClient();
    final String CASE_DATA_URL = "http://localhost:3000/casedata";
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @Override
    public String createCaseData(OnboardingCaseData onboardingCaseData) {
        String parentWorkflowId = onboardingCaseData.getWorkflowId();
        RequestBody body = RequestBody.create(JSON, new Gson().toJson(onboardingCaseData));
        Request request = new Request.Builder()
                .url(CASE_DATA_URL + "/" + parentWorkflowId).post(body)
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String updateCaseData(OnboardingCaseData onboardingCaseData) {
        String parentWorkflowId = onboardingCaseData.getWorkflowId();
        RequestBody body = RequestBody.create(JSON, new Gson().toJson(onboardingCaseData));
        Request request = new Request.Builder()
                .url(CASE_DATA_URL + "/" + parentWorkflowId).put(body)
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String updateDocument(OnboardingCaseDataDocuments onboardingCaseDataDocuments, String workflowId) {
        String documentType = null;
        Object reqBody = null;
        if (onboardingCaseDataDocuments.getPostEligibility() != null) {
            documentType = onboardingCaseDataDocuments.getPostEligibility().getDocumentType().getType();
            reqBody = onboardingCaseDataDocuments.getPostEligibility();
        }
        if (onboardingCaseDataDocuments.getCustomerEvaluation() != null) {
            documentType = onboardingCaseDataDocuments.getCustomerEvaluation().getDocumentType().getType();
            reqBody = onboardingCaseDataDocuments.getCustomerEvaluation();
        }
        if (onboardingCaseDataDocuments.getDocumentPreparation() != null) {
            documentType = onboardingCaseDataDocuments.getDocumentPreparation().getDocumentType().getType();
            reqBody = onboardingCaseDataDocuments.getDocumentPreparation();
        }
        if (onboardingCaseDataDocuments.getAccountFulfillment() != null) {
            documentType = onboardingCaseDataDocuments.getAccountFulfillment().getDocumentType().getType();
            reqBody = onboardingCaseDataDocuments.getAccountFulfillment();
        }
        if (onboardingCaseDataDocuments.getCardFulfillment() != null) {
            documentType = onboardingCaseDataDocuments.getCardFulfillment().getDocumentType().getType();
            reqBody = onboardingCaseDataDocuments.getCardFulfillment();
        }

        //String parentWorkflowId = Workflow.getInfo().getParentWorkflowId().get();
        RequestBody body = RequestBody.create(JSON, new Gson().toJson(reqBody));
        Request request = new Request.Builder()
                .url(CASE_DATA_URL + "/" + workflowId + "/document/" + documentType).put(body)
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
