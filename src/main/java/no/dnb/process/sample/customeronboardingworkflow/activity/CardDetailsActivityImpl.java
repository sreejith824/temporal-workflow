package no.dnb.process.sample.customeronboardingworkflow.activity;

import no.dnb.process.sample.customeronboardingworkflow.domain.Customer;

import java.util.Random;

public class CardDetailsActivityImpl implements CardDetailsActivity {
    @Override
    public String createCard(Customer customer) {
        Random rand = new Random();
        return "CARD" + String.valueOf(rand.nextInt(100000000));
    }

}
