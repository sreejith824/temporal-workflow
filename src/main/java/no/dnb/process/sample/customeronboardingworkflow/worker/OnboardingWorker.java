package no.dnb.process.sample.customeronboardingworkflow.worker;

import io.temporal.client.WorkflowClient;
import io.temporal.client.WorkflowOptions;
import io.temporal.serviceclient.WorkflowServiceStubs;
import io.temporal.worker.Worker;
import io.temporal.worker.WorkerFactory;
import no.dnb.process.sample.customeronboardingworkflow.activity.*;
import no.dnb.process.sample.customeronboardingworkflow.domain.Customer;
import no.dnb.process.sample.customeronboardingworkflow.workflow.*;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;


@Service 
public class OnboardingWorker {
    // Define the task queue name
    static final String TASK_QUEUE = "OnboardingTaskQueue";

    WorkflowClient client = WorkflowClient.newInstance(WorkflowServiceStubs.newLocalServiceStubs());
    WorkerFactory workerFactory = WorkerFactory.newInstance(client);
    Worker worker = workerFactory.newWorker(TASK_QUEUE);
    OnboardingWorkflow onboardingWorkflow;

    public OnboardingWorker() {
        worker.registerWorkflowImplementationTypes(
                OnboardingWorkflowImpl.class,
                PostEligibilityWorkflowImpl.class,
                CustomerEvaluationWorkflowImpl.class,
                DocumentPreparationWorkflowImpl.class,
                AccountFulfillmentWorkflowImpl.class,
                CardFulfillmentWorkflowImpl.class);
        worker.registerActivitiesImplementations(
                new CustomerDetailsActivityImpl(),
                new ProductDetailsActivityImpl(),
                new CustomerScoringActivityImpl(),
                new DocumentProductionActivityImpl(),
                new AccountDetailsActivityImpl(),
                new CardDetailsActivityImpl(),
                new CaseDataActivityImpl());
        workerFactory.start();
    }

    public String startCustomerOnboardingWorkflow(Customer customer) {
        System.out.println(customer.name());
        System.out.println(customer.ssn());
        System.out.println(customer.productName());
        onboardingWorkflow = client.newWorkflowStub(OnboardingWorkflow.class, WorkflowOptions.newBuilder()
                .setTaskQueue(TASK_QUEUE)
                        .setWorkflowExecutionTimeout(Duration.ofMinutes(10))
                .build());
        return "http://localhost:9090/pm-digital/api/v1/process/workflowid/" +
                WorkflowClient.start(onboardingWorkflow::onboardCustomer, customer).getWorkflowId() +
                "/status";
    }

    public String getWorkflowStatus(String workflowId) {
        onboardingWorkflow = client.newWorkflowStub(OnboardingWorkflow.class, workflowId);
        return onboardingWorkflow.getStatus();
    }

}
