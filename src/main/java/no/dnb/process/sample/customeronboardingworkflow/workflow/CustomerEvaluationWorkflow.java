package no.dnb.process.sample.customeronboardingworkflow.workflow;

import io.temporal.workflow.QueryMethod;
import io.temporal.workflow.WorkflowInterface;
import io.temporal.workflow.WorkflowMethod;
import no.dnb.process.sample.casedata.domain.OnboardingCaseData;
import no.dnb.process.sample.customeronboardingworkflow.domain.CustomerEvaluation;

@WorkflowInterface
public interface CustomerEvaluationWorkflow {
    @WorkflowMethod
    void executeCustomerEvaluation(CustomerEvaluation customerEvaluation, OnboardingCaseData onboardingCaseData);
    @QueryMethod
    String getStatus();
}
