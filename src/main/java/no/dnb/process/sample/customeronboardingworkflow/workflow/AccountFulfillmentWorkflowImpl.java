package no.dnb.process.sample.customeronboardingworkflow.workflow;

import io.temporal.activity.ActivityOptions;
import io.temporal.workflow.Workflow;
import no.dnb.process.sample.casedata.domain.OnboardingCaseData;
import no.dnb.process.sample.casedata.domain.OnboardingCaseDataDocuments;
import no.dnb.process.sample.casedata.domain.documents.AccountFulfillmentDocument;
import no.dnb.process.sample.casedata.domain.documents.CardFulfillmentDocument;
import no.dnb.process.sample.customeronboardingworkflow.activity.AccountDetailsActivity;
import no.dnb.process.sample.customeronboardingworkflow.activity.CaseDataActivity;
import no.dnb.process.sample.customeronboardingworkflow.domain.AccountFulfillmentWorkflowStatus;
import no.dnb.process.sample.customeronboardingworkflow.domain.Customer;

import java.time.Duration;

public class AccountFulfillmentWorkflowImpl implements AccountFulfillmentWorkflow {

    final AccountDetailsActivity accountDetailsActivity =
            Workflow.newActivityStub(
                    AccountDetailsActivity.class,
                    ActivityOptions.newBuilder().setStartToCloseTimeout(Duration.ofSeconds(2)).build());
    final CaseDataActivity caseDataActivity =
            Workflow.newActivityStub(
                    CaseDataActivity.class,
                    ActivityOptions.newBuilder().setStartToCloseTimeout(Duration.ofSeconds(2)).build());

    String status;

    @Override
    public void executeAccountFulfillment(Customer customer, OnboardingCaseData onboardingCaseData) {
        status = AccountFulfillmentWorkflowStatus.ACCOUNT_CREATION_IN_PROGRESS.getStatus();
        WorkflowUtil.updateParentWorkflowStatus(status);
        Workflow.await(Duration.ofMinutes(1), () -> false);
        String accountNumber = accountDetailsActivity.createAccount(customer);
        status = AccountFulfillmentWorkflowStatus.ACCOUNT_CREATION_COMPLETED.getStatus();
        WorkflowUtil.updateParentWorkflowStatus(status);

        String parentWorkflowId = Workflow.getInfo().getParentWorkflowId().get();
        OnboardingCaseDataDocuments onboardingCaseDataDocuments = new OnboardingCaseDataDocuments();
        onboardingCaseDataDocuments.setAccountFulfillment(new AccountFulfillmentDocument(accountNumber));
        caseDataActivity.updateDocument(onboardingCaseDataDocuments, parentWorkflowId);

    }

    @Override
    public String getStatus() {
        return status;
    }
}
