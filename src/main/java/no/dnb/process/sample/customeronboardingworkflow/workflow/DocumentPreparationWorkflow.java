package no.dnb.process.sample.customeronboardingworkflow.workflow;

import io.temporal.workflow.QueryMethod;
import io.temporal.workflow.WorkflowInterface;
import io.temporal.workflow.WorkflowMethod;
import no.dnb.process.sample.casedata.domain.OnboardingCaseData;

@WorkflowInterface
public interface DocumentPreparationWorkflow {
    @WorkflowMethod
    void executeDocumentPreparation(String name, String ssn, String productName, OnboardingCaseData onboardingCaseData);
    @QueryMethod
    String getStatus();
}
