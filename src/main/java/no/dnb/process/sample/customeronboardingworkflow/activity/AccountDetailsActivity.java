package no.dnb.process.sample.customeronboardingworkflow.activity;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;
import no.dnb.process.sample.customeronboardingworkflow.domain.Customer;

@ActivityInterface
public interface AccountDetailsActivity {
    @ActivityMethod
    String createAccount(Customer customer);
}
