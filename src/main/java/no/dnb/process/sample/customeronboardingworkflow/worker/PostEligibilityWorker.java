package no.dnb.process.sample.customeronboardingworkflow.worker;

import io.temporal.client.WorkflowClient;
import io.temporal.client.WorkflowOptions;
import io.temporal.serviceclient.WorkflowServiceStubs;
import io.temporal.worker.Worker;
import io.temporal.worker.WorkerFactory;
import no.dnb.process.sample.casedata.domain.OnboardingCaseData;
import no.dnb.process.sample.customeronboardingworkflow.activity.CustomerDetailsActivityImpl;
import no.dnb.process.sample.customeronboardingworkflow.activity.ProductDetailsActivityImpl;
import no.dnb.process.sample.customeronboardingworkflow.workflow.PostEligibilityWorkflow;
import no.dnb.process.sample.customeronboardingworkflow.workflow.PostEligibilityWorkflowImpl;

public class PostEligibilityWorker {
    static final String TASK_QUEUE = "PostEligibilityTaskQueue";
    WorkflowClient client = WorkflowClient.newInstance(WorkflowServiceStubs.newLocalServiceStubs());
    WorkerFactory workerFactory = WorkerFactory.newInstance(client);
    Worker worker = workerFactory.newWorker(TASK_QUEUE);


    public String startPostEligibilityWorkflowWorker(String name, String ssn, String productName) {
        worker.registerWorkflowImplementationTypes(PostEligibilityWorkflowImpl.class);
        worker.registerActivitiesImplementations(new CustomerDetailsActivityImpl());
        worker.registerActivitiesImplementations(new ProductDetailsActivityImpl());
        PostEligibilityWorkflow postEligibilityWorkflow = client.newWorkflowStub(PostEligibilityWorkflow.class, WorkflowOptions.newBuilder()
                .setTaskQueue(TASK_QUEUE)
                .build());
        workerFactory.start();
        return WorkflowClient.start((name1, ssn1, productName1) -> {
            postEligibilityWorkflow.executePostEligibility(name1, ssn1, productName1, new OnboardingCaseData());
        }, name, ssn, productName).getWorkflowId();
    }
}
