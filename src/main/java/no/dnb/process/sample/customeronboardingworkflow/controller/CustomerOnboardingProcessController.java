package no.dnb.process.sample.customeronboardingworkflow.controller;

import no.dnb.process.sample.customeronboardingworkflow.domain.Customer;
import no.dnb.process.sample.customeronboardingworkflow.worker.OnboardingWorker;
import no.dnb.process.sample.customeronboardingworkflow.worker.PostEligibilityWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("api/v1/process")
@RestController
public class CustomerOnboardingProcessController {
    @Autowired
    OnboardingWorker onboardingWorker;
    @PostMapping("/workflowtype/{processName}")
    public ResponseEntity<String> startProcess(@PathVariable("processName") String processName, @RequestBody Customer customer) {
        if ("OnboardingWorkflow".equals(processName)) {
            return new ResponseEntity<String>(onboardingWorker.startCustomerOnboardingWorkflow(customer), HttpStatus.OK);
        } else if ("PostEligibility".equals(processName)) {
            PostEligibilityWorker postEligibilityWorker = new PostEligibilityWorker();
            return new ResponseEntity<String>(postEligibilityWorker.startPostEligibilityWorkflowWorker("Sreejith", "123456", "SAGA"), HttpStatus.OK);

        }
        return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/workflowid/{workflowid}/status")
    public ResponseEntity<String> get(@PathVariable("workflowid") String workflowid ) {
        return new ResponseEntity<>(onboardingWorker.getWorkflowStatus(workflowid), HttpStatus.OK);
    }

}
