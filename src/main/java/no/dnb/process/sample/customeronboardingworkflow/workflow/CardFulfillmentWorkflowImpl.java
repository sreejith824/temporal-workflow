package no.dnb.process.sample.customeronboardingworkflow.workflow;

import io.temporal.activity.ActivityOptions;
import io.temporal.workflow.Workflow;
import no.dnb.process.sample.casedata.domain.OnboardingCaseData;
import no.dnb.process.sample.casedata.domain.OnboardingCaseDataDocuments;
import no.dnb.process.sample.casedata.domain.documents.CardFulfillmentDocument;
import no.dnb.process.sample.customeronboardingworkflow.activity.CardDetailsActivity;
import no.dnb.process.sample.customeronboardingworkflow.activity.CaseDataActivity;
import no.dnb.process.sample.customeronboardingworkflow.domain.CardFulfillmentWorkflowStatus;
import no.dnb.process.sample.customeronboardingworkflow.domain.Customer;

import java.time.Duration;

public class CardFulfillmentWorkflowImpl implements CardFulfillmentWorkflow {

    final CardDetailsActivity cardDetailsActivity =
            Workflow.newActivityStub(
                    CardDetailsActivity.class,
                    ActivityOptions.newBuilder().setStartToCloseTimeout(Duration.ofSeconds(2)).build());
    final CaseDataActivity caseDataActivity =
            Workflow.newActivityStub(
                    CaseDataActivity.class,
                    ActivityOptions.newBuilder().setStartToCloseTimeout(Duration.ofSeconds(2)).build());

    String status;

    @Override
    public void executeAccountFulfillment(Customer customer, OnboardingCaseData onboardingCaseData) {
        status = CardFulfillmentWorkflowStatus.CARD_CREATION_IN_PROGRESS.getStatus();
        WorkflowUtil.updateParentWorkflowStatus(status);
        Workflow.await(Duration.ofMinutes(1), () -> false);
        String cardNumber = cardDetailsActivity.createCard(customer);
        status = CardFulfillmentWorkflowStatus.CARD_CREATION_COMPLETED.getStatus();
        WorkflowUtil.updateParentWorkflowStatus(status);

        String parentWorkflowId = Workflow.getInfo().getParentWorkflowId().get();
        OnboardingCaseDataDocuments onboardingCaseDataDocuments = new OnboardingCaseDataDocuments();
        onboardingCaseDataDocuments.setCardFulfillment(new CardFulfillmentDocument(cardNumber));
        caseDataActivity.updateDocument(onboardingCaseDataDocuments, parentWorkflowId);
    }

    @Override
    public String getStatus() {
        return status;
    }
}
