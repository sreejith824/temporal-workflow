package no.dnb.process.sample.customeronboardingworkflow.activity;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;
import no.dnb.process.sample.casedata.domain.OnboardingCaseData;
import no.dnb.process.sample.casedata.domain.OnboardingCaseDataDocuments;
import no.dnb.process.sample.casedata.domain.documents.CaseDataDocument;
import no.dnb.process.sample.casedata.domain.documents.CaseDataDocumentType;
import no.dnb.process.sample.customeronboardingworkflow.domain.Customer;

@ActivityInterface
public interface CaseDataActivity {
    @ActivityMethod
    String createCaseData(OnboardingCaseData onboardingCaseData);

    String updateCaseData(OnboardingCaseData onboardingCaseData);

    String updateDocument(OnboardingCaseDataDocuments onboardingCaseDataDocuments, String workflowId);

}
