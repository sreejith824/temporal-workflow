package no.dnb.process.sample.customeronboardingworkflow.activity;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;

@ActivityInterface
public interface ProductDetailsActivity {
    @ActivityMethod
    String checkProductEligibility(String productName);
}
