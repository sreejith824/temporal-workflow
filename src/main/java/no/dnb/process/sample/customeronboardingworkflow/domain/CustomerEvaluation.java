package no.dnb.process.sample.customeronboardingworkflow.domain;

public record CustomerEvaluation (
        String name,
        int age,
        String nationality,
        int income
) {
}
