package no.dnb.process.sample.customeronboardingworkflow.domain;

public enum CustomerOnboardingWorkflowStatus {
    CUSTOMER_ONBOARDING_STARTED("CUSTOMER_ONBOARDING_STARTED"),

    CUSTOMER_ONBOARDING_PENDING("CUSTOMER_ONBOARDING_PENDING"),
    CUSTOMER_ONBOARDING_COMPLETED("CUSTOMER_ONBOARDING_COMPLETED"),
    CUSTOMER_ONBOARDING_CANCELLED("CUSTOMER_ONBOARDING_CANCELLED"),
    CUSTOMER_ONBOARDING_FAILED("CUSTOMER_ONBOARDING_FAILED"),
    KYC_PENDING("KYC_PENDING"),
    USER_CONFIRMATION_PENDING("USER_CONFIRMATION_PENDING"),
    WAITING_FOR_ESIGN("WAITING_FOR_ESIGN");

    private final String status;

    CustomerOnboardingWorkflowStatus(String status) {
        this.status = status;

    }

    public String getStatus() {
        return status;
    }
}
