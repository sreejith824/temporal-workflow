package no.dnb.process.sample.customeronboardingworkflow.workflow;

import io.temporal.activity.ActivityOptions;
import io.temporal.workflow.Workflow;
import no.dnb.process.sample.casedata.domain.OnboardingCaseData;
import no.dnb.process.sample.casedata.domain.OnboardingCaseDataDocuments;
import no.dnb.process.sample.casedata.domain.documents.CustomerEvaluationDocument;
import no.dnb.process.sample.casedata.domain.documents.PostEligibilityDocument;
import no.dnb.process.sample.customeronboardingworkflow.activity.CaseDataActivity;
import no.dnb.process.sample.customeronboardingworkflow.activity.CustomerScoringActivity;
import no.dnb.process.sample.customeronboardingworkflow.domain.CustomerEvaluation;
import no.dnb.process.sample.customeronboardingworkflow.domain.CustomerEvaluationWorkflowStatus;

import java.time.Duration;

public class CustomerEvaluationWorkflowImpl implements CustomerEvaluationWorkflow {

    final CustomerScoringActivity customerScoringActivity =
            Workflow.newActivityStub(
                    CustomerScoringActivity.class,
                    ActivityOptions.newBuilder().setStartToCloseTimeout(Duration.ofSeconds(2)).build());
    final CaseDataActivity caseDataActivity =
            Workflow.newActivityStub(
                    CaseDataActivity.class,
                    ActivityOptions.newBuilder().setStartToCloseTimeout(Duration.ofSeconds(2)).build());

    String status;

    @Override
    public void executeCustomerEvaluation(CustomerEvaluation customerEvaluation, OnboardingCaseData onboardingCaseData) {
        status =  CustomerEvaluationWorkflowStatus.CUSTOMER_EVALUATION_STARTED.getStatus();
        WorkflowUtil.updateParentWorkflowStatus(status);

        String id = customerScoringActivity.submitForCustomerScoring(customerEvaluation);
        status =  CustomerEvaluationWorkflowStatus.AML_SCORING_IN_PROGRESS.getStatus();
        WorkflowUtil.updateParentWorkflowStatus(status);
        Workflow.await(Duration.ofMinutes(1),() -> false);


        String score = customerScoringActivity.getCustomerScoring(id);
        status = ("APPROVED".equals(score) ? CustomerEvaluationWorkflowStatus.CUSTOMER_EVALUATION_COMPLETED.getStatus() :
                CustomerEvaluationWorkflowStatus.CUSTOMER_EVALUATION_FAILED.getStatus());
        WorkflowUtil.updateParentWorkflowStatus(status);
        String parentWorkflowId = Workflow.getInfo().getParentWorkflowId().get();
        OnboardingCaseDataDocuments onboardingCaseDataDocuments = new OnboardingCaseDataDocuments();
        onboardingCaseDataDocuments.setCustomerEvaluation(new CustomerEvaluationDocument(customerEvaluation, score));
        caseDataActivity.updateDocument(onboardingCaseDataDocuments, parentWorkflowId);
    }


    @Override
    public String getStatus() {
        return status;
    }
}
