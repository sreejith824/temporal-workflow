package no.dnb.process.sample.customeronboardingworkflow.activity;

public class DocumentProductionActivityImpl implements DocumentProductionActivity {
    @Override
    public String produceDocument(String name, String ssn, String productName) {
        return "Document Produced for : " + name + " : " + " : " + ssn + " : " + productName;
    }
}
